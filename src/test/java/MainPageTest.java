import helpers.Driver;
import io.qameta.allure.Allure;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.List;

class MainPageTest extends BaseTest {
    private static final int MAX_COUNT_TEST = 3;

@Test
void сlickAllBrand() throws FileNotFoundException {
    List<String> brands =  app.getMainPage().open().getBrandsName();
    int clickCount=0;
    for (String brand:brands) {
        Assertions.assertTrue(app.getMainPage().open().
                clickBrand(brand).
                titleBrandePage.
                getText().contains(brand));
        Assertions.assertTrue(app.getFiltersPage().
                getSetValueBrandComboBox().
                contains(brand));
        Allure.addAttachment("SCREEN CLICK HREF "  + brand,
                Driver.takeScreenshot("SCREEN CLICK HREF "  + brand));
        clickCount++;
        if (clickCount>=MAX_COUNT_TEST){
            break;
        }

    }
}

}
