import app.App;
import helpers.Driver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import util.TestListener;

@ExtendWith(TestListener.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BaseTest {
    protected App app;

    @BeforeAll
    public void setUp() {
        Driver.initDriver();
        app = new App();
        app.getMainPage().open();
    }

    @AfterEach
    public void clearCookies() {
        Driver.clearCookies();
    }

    @AfterAll
    public void closeDriver() {
        Driver.close();
    }

}