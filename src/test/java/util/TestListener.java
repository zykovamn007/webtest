package util;

import helpers.Driver;
import io.qameta.allure.Allure;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

import java.io.FileNotFoundException;

public class TestListener implements TestWatcher {

    @Override
    public void  testFailed(ExtensionContext context, Throwable cause) {
        try {
            Allure.addAttachment("SCREEN FAILED",
                    Driver.takeScreenshot());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Allure.step("test failed");

    }

    @Override
    public void testSuccessful (ExtensionContext context)  {
        Allure.step("test successful");

    }


}

