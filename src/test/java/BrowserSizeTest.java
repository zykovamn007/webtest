import helpers.Driver;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class BrowserSizeTest extends BaseTest{

    @ParameterizedTest
    @CsvSource({"600, 300", "980, 740", "400, 1024"})
    void diffWindowsTesting(Integer width, Integer height)  {
        Driver.setSizeBrowser(width,height);
        Boolean heightScroll = (Boolean) Driver.executeJsReturn("return document.documentElement.scrollHeight>document.documentElement.clientHeight");
        Boolean widthScroll = (Boolean) Driver.executeJsReturn("return document.documentElement.scrollWidth>document.documentElement.clientWidth");
        assert (heightScroll);
        assert (widthScroll);
    }

}
