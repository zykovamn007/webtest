import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

class LoginTest extends BaseTest {
    ObjectMapper mapper = new ObjectMapper();

    @Test
    void login() throws IOException {
        InputStream is = this.getClass().getClassLoader()
                .getResourceAsStream(File.separator + "data" + File.separator + "user.json");
        User user  = mapper.readValue(is, User.class);
        app.getMainPage().open();
        app.getLandingPage().loggedInSuccessfully(user).checkPhone(user.getPhone());
    }


    @ParameterizedTest
    @CsvFileSource(resources =  "data\\wrong_email.csv")
    void loginWrongEmail(final String incorrectEmail, final String errorMessage) throws InterruptedException {
        app.getMainPage().open();
        Assertions.assertEquals(app.getLandingPage().
                clickLogin().
                fillEmail(incorrectEmail == null ? "" : incorrectEmail).
                clickSignInBtn().
                getErrorTextLogin(),
                errorMessage);
    }

    @ParameterizedTest
    @MethodSource("wrongPassword")
    void loginWrongPassword(final String incorrectPassword, final String errorMessage) throws  IOException {
        app.getMainPage().open();
        InputStream is = this.getClass().getClassLoader()
                .getResourceAsStream(File.separator + "data" + File.separator + "user.json");
        User user  = mapper.readValue(is, User.class);
        Assertions.assertEquals(app.getLandingPage().
                        clickLogin().
                        fillEmail(user.getUsername()).
                        fillPassword(incorrectPassword == null ? "" : incorrectPassword).
                        clickSignInBtn().
                        getErrorTextPass(),
                errorMessage);
    }

    private static Stream<Arguments> wrongPassword() {
        return Stream.of(
                Arguments.of("", "Данные для входа неверны"),
                Arguments.of("123", "Данные для входа неверны"));
    }


    @Test
    void compareImage () throws IOException {
        app.getMainPage().open();
        Assertions.assertFalse(app.getLandingPage().
                clickLogin().checkIsDifferByImage("ExpectedScreenLogin.png"));
    }
}