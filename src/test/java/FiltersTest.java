import helpers.Driver;
import io.qameta.allure.Allure;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.FileNotFoundException;
import java.util.List;

class FiltersTest extends BaseTest {

    @ParameterizedTest
    @CsvSource(value = {
            "Ford,Focus,2019, 2022",
            "Acura,RDX,2000,2022",
            "Alpina,B4,2000,2022"}
    )
    void findByBrandModelYear(String brand, String model, int yearFrom, int yearTo) throws  FileNotFoundException {
        Allure.step("testng search by brand, model, year");
        List<String> titles =
                app.getFiltersPage().open()
                .selectBrand(brand)
                .selectModel(model)
                .selectYearFrom(yearFrom)
                .selectYearTo(yearTo)
                .checkUnSold()
                .clickShow()
                .getTextResultTitle();
        for (String title: titles) {
            Assertions.assertTrue(title.toLowerCase().contains(brand.toLowerCase()));
            Assertions.assertTrue(title.toLowerCase().contains(model.toLowerCase()));
            int yearActual = Integer.valueOf(title.substring(title.indexOf(",")+1,title.length()).trim());
            Assertions.assertTrue(yearActual<= yearTo && yearActual>= yearFrom);
        }
        Allure.addAttachment("SCREEN FIND "  + brand + " " + model,
                Driver.takeScreenshot("SCREEN FIND "  + brand + " " + model));
    }
    @Test
    void emptyResult()  {
        Allure.step("testng search by price");
        Assertions.assertTrue(app.getFiltersPage().open()
                .selectBrand("Ford")
                .selectModel("Focus")
                .selectYearFrom(1980)
                .selectYearTo(1980)
                .clickShow()
                .existWarningMessage());
    }

    @Test
    void findByPrice(){
        Allure.step("test successful");
        List<String> prices =
                app.getFiltersPage().open()
                .selectPriceFrom(25000)
                .selectPriceTo(65000)
                .clickShow()
                .getTextResultPrice();

        for (String price: prices) {
            int priceActual = Integer.valueOf(price.replace(" ",""));
            Assertions.assertTrue(priceActual<= 65000 && priceActual>= 25000);
        }
    }


}
