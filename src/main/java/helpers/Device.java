package helpers;

public enum Device {

    desktop("1920x1080"),
    mobile ("320x480"),
    table ("768x1024");

    private String size;

    Device( String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }
}
