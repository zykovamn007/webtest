package helpers;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;


public class Driver {

    private static ConfigWebDriver config = ConfigWebDriver.getConfig();

    public static void initDriver() {
        Configuration.browser = config.getBrowser();
        Configuration.pageLoadStrategy = config.getPageLoadStrategy();
        Configuration.browserSize = config.getBrowserSize();
        Configuration.holdBrowserOpen = config.getHoldBrowserOpen();
        Configuration.screenshots = config.getScreenshots();
        Configuration.pageLoadTimeout = config.getPageLoadTimeout();
        Configuration.timeout = config.getTimeout();
        Configuration.headless = config.getHeadless();
        if (config.getReportsFolder()!=null){
            Configuration.reportsFolder = config.getReportsFolder();
        }

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );

    }
    public static WebDriver currentDriver() {
        return WebDriverRunner.getWebDriver();
    }

    public static void open(String url) {
        Selenide.open(url);
    }
    public static void open() {
        Selenide.open();
    }

    public static void refresh() {
        Selenide.refresh();
    }

    public static void executeJs(String script) {

        try {
            Selenide.executeJavaScript(script);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object executeJsReturn(String script) {

        try {
            return Selenide.executeJavaScript(script);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static void waitForUrlContains(String urlChunk, int seconds) {
        WebDriverWait wait = new WebDriverWait(currentDriver(), Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.urlContains(urlChunk));
    }

    public static void waitForUrlDoesNotContain(String urlChunk, int maxTime) {
        while(  currentDriver().getCurrentUrl().contains(urlChunk)  && maxTime > 0) {
            wait(1);
            maxTime--;
        }
    }


    public static void waitVisible(SelenideElement element, int seconds){
        WebDriverWait wait = new WebDriverWait(currentDriver(), Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitClickable(SelenideElement element, int seconds) {
        WebDriverWait wait = new WebDriverWait(currentDriver(), Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }


        public static void clearCookies() {
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
    }

    public static void close() {
        Selenide.closeWebDriver();
    }


    public static void wait(int seconds)
    {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static String takeScreenshot() throws FileNotFoundException {
        return takeScreenshot("screenshot_" +
                (new SimpleDateFormat("HHmmssSSS").format(new Date())));
    }

    public static String takeScreenshot(String nameScreen) throws FileNotFoundException {
        return Selenide.screenshot("screenshots" + File.separator + nameScreen);
    }

    public static void waitFor(By waitingElementLocator, int seconds) {
        WebDriverWait wait = new WebDriverWait(currentDriver(),  Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.visibilityOfElementLocated(waitingElementLocator));
    }

    public static void waitForNot(By waitingElementLocator, int seconds) {
        WebDriverWait wait = new WebDriverWait(currentDriver(), Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOfElementLocated(waitingElementLocator)));
    }

    public static boolean isElementExists(By elementLocator, int seconds) {
        try {
            waitFor(elementLocator, seconds);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isElementNotExists(By elementLocator, int seconds) {
        try {
            waitForNot(elementLocator, seconds);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void setSizeBrowser (Integer width, Integer height){
        currentDriver().manage().window().setSize(new org.openqa.selenium.Dimension(width, height));
    }

    public static void scrollByXpass (String path){
        executeJs("document.evaluate(" + path + "" +
                ", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView()");
    }


}
