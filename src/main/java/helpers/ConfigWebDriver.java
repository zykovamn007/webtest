package helpers;

import com.codeborne.selenide.Browsers;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.Data;

import java.io.File;

@Data
public class ConfigWebDriver {

    private static ConfigWebDriver configWebDriver;
    private Config config;
    private String browser;
    private Boolean headless;
    private String pageLoadStrategy;
    private Boolean holdBrowserOpen;
    private Boolean screenshots;
    private int pageLoadTimeout;
    private int timeout;
    private String reportsFolder;
    private String browserSize;

    public static ConfigWebDriver getConfig(){
        if(configWebDriver == null)
            configWebDriver = new ConfigWebDriver();

        return configWebDriver;
    }
    private  ConfigWebDriver(){
        config = ConfigFactory.load("SettingBrowser");

        if (config.hasPathOrNull ("browser")){
            switch (config.getString ("browser")) {
                case  ("chrome"):
                    browser = Browsers.CHROME;
                    break;
                case ("firefox"):
                    browser = Browsers.FIREFOX;
                    break;
                case ("internet explorer"):
                    browser = Browsers.INTERNET_EXPLORER;
                    break;
            }
        }else{
            browser = Browsers.CHROME;
        }

        headless = config.hasPathOrNull ("headless") ? config.getBoolean ("headless") : false;
        pageLoadStrategy = config.hasPathOrNull ("pageLoadStrategy") ? config.getString ("pageLoadStrategy") : "eager" ;
        holdBrowserOpen = config.hasPathOrNull ("holdBrowserOpen") ? config.getBoolean ("holdBrowserOpen") :false ;
        screenshots = config.hasPathOrNull ("screenshots") ? config.getBoolean ("screenshots") :false;
        reportsFolder = config.hasPathOrNull ("reportsFolder") ? System.getProperty("user.dir")
                + File.separator +  config.getString ("reportsFolder") : null;
        pageLoadTimeout = config.hasPathOrNull ("pageLoadTimeout") ? config.getInt ("pageLoadTimeout"): 1000;
        timeout = config.hasPathOrNull ("timeout") ? config.getInt ("timeout"): 1000;

        if (config.hasPathOrNull ("device")){
            browserSize = Device.valueOf(config.getString ("device")).getSize();
        }else{
            browserSize ="1920x1080";
        }

    }

}
