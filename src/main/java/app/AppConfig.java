package app;

public class AppConfig {

    private AppConfig() {
    }

    public static final String BASEURL = "https://www.drom.ru/";
    public static final String AUTOURL = "https://auto.drom.ru/";
    public static final int MAX_CHECK_PAGE = 3;

}


