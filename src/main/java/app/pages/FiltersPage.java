package app.pages;

import app.AppConfig;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import helpers.Driver;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$x;

public class FiltersPage {

    public SelenideElement brandComboBox = $x("//*[@data-ftid='sales__filter_fid']")
            .$x(".//*[@role='combobox']");
    public SelenideElement modelComboBox = $x("//*[@data-ftid='sales__filter_mid']")
            .$x(".//*[@role='combobox']");
    public SelenideElement priceFromComboBox = $x("//*[@data-ftid='sales__filter_price-from']");
    public SelenideElement priceToComboBox= $x("//*[@data-ftid='sales__filter_price-to']");
    public SelenideElement yearFromComboBox =$x("//*[@data-ftid='sales__filter_year-from']")
            .$x(".//*[@role='combobox']");
    public SelenideElement yearToComboBox= $x("//*[@data-ftid='sales__filter_year-to']")
            .$x(".//*[@role='combobox']");
    public SelenideElement UnSoldCheckBox =$x(".//*[@for='sales__filter_unsold']");
    public SelenideElement showButton = $x("//*[@data-ftid='sales__filter_submit-button']")
            .$x(".//*[contains(text(),'Показать')]");


    public FiltersPage open() {
        Selenide.open(AppConfig.AUTOURL);
        return this;
    }

    public String getSetValueBrandComboBox(){
        return brandComboBox.getAttribute("placeholder");
    }

    public String getSetValueModelComboBox(){
        return modelComboBox.getAttribute("placeholder");
    }

    @Step ("select Brand")
    public FiltersPage selectBrand(String brand){
        Driver.waitVisible(brandComboBox,20);
        brandComboBox.click();
        brandComboBox.sendKeys(brand);
        String locatorBrandItem = "//*[@data-ftid='sales__filter_fid']//*[@data-ftid='component_select_dropdown']//*[contains(text(),'" + brand + "')]";
        if (!Driver.isElementExists(By.xpath(locatorBrandItem),5)){
            brandComboBox.click();
        }
        SelenideElement brandItem = $x(locatorBrandItem);
        Driver.waitVisible(brandItem,20);
        brandItem.click();
        return this;
    }
    @Step ("select Model")
    public FiltersPage selectModel(String model){
        Driver.waitVisible(modelComboBox,20);
        modelComboBox.click();
        modelComboBox.sendKeys(model);
        String locatorModelItem = "//*[@data-ftid='sales__filter_mid']//*[@data-ftid='component_select_dropdown']//*[contains(text(),'" + model + "')]";
        if (!Driver.isElementExists(By.xpath(locatorModelItem),5)){
            modelComboBox.click();
        }
        SelenideElement modelItem = $x(locatorModelItem);
        Driver.waitVisible(modelItem,20);
        modelItem.click();
        return this;
    }

    @Step ("select Year From")
    public FiltersPage selectYearFrom(int year){
        yearFromComboBox.click();
        $x("//*[@data-ftid='sales__filter_year-from']").
                $x(".//*[@data-ftid='component_select_dropdown']").
                $x(".//*[contains(text(),'" + year + "')]").click();
        return this;
    }

    @Step ("select Year To")
    public FiltersPage selectYearTo(int year){
        yearToComboBox.click();
        $x("//*[@data-ftid='sales__filter_year-to']").
                $x(".//*[@data-ftid='component_select_dropdown']").
                $x(".//*[contains(text(),'" + year + "')]").click();
        return this;
    }

    @Step ("select Price From")
    public FiltersPage selectPriceFrom(int price){
        priceFromComboBox.sendKeys(String.valueOf(price));
        return this;
    }

    @Step ("select Price To")
    public FiltersPage selectPriceTo(int price){
        priceToComboBox.sendKeys(String.valueOf(price));
        return this;
    }

    @Step ("check UnSold")
    public FiltersPage checkUnSold(){
        UnSoldCheckBox.click();
        return this;
    }

    @Step ("click Show")
    public ResultPage clickShow(){
        showButton.click();
        return new ResultPage();
    }

}
