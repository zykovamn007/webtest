package app.pages;

import helpers.StringHelper;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SmsVerificationPage {

    public Boolean checkPhone(String phone){
        return StringHelper.replaceForPhone($(By.tagName("nobr")).
                getText()).
                contains(StringHelper.replaceForPhone(phone));
    }
}
