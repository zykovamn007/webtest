package app.pages;

import app.AppConfig;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage  {

    SelenideElement showAllBrande = $x("//*[contains(text(),'Показать все')]");

    public MainPage open() {
        Selenide.open(AppConfig.BASEURL);
        return this;
    }

    @Step("click Brand")
    public ResultPage clickBrand(String brand){
        showAllBrande.click();
        $x(".//a[contains(text(),'" + brand +
                "')][@data-ftid='component_cars-list-item_hidden-link']").click();
        return new ResultPage();
    }

    public List<String>  getBrandsName(){
        showAllBrande.click();
        ElementsCollection brands =$$x(".//a[@data-ftid='component_cars-list-item_hidden-link']");
        List<String> nameBrands = new ArrayList<String>();
        for (SelenideElement brand:brands) {
            nameBrands.add(brand.text());
        }
        return nameBrands;
    }

}
