package app.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import model.User;

import static com.codeborne.selenide.Selenide.$x;

public class LandingPage {
    public SelenideElement loginHref  = $x("//a[@data-ftid='component_header_login']");
    public SelenideElement autoHref  = $x("//a[@data-ftid='component_header_main-menu-item']");


    public SmsVerificationPage loggedInSuccessfully(User user){
        return loggedInSuccessfully(user.getUsername(), user.getPassword());

    }
    public SmsVerificationPage loggedInSuccessfully(String username, String password){
        loginHref.click();
        return new LoginPage().login(username, password);

    }
    @Step ("click Login")
    public LoginPage clickLogin (){
        loginHref.click();
        return new LoginPage();
    }

}
