package app.pages;

import app.AppConfig;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import helpers.Driver;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class ResultPage {
    public SelenideElement titleBrandePage = $x("//h1[contains(text(),'Продажа ')]");
    private By warningLocator=  By.xpath(".//*[@data-ftid='component_notification_type_warning']");
    private By hrefNextLocator = By.xpath(".//a[@data-ftid='component_pagination-item-next']");
    public SelenideElement hrefNext = $x(".//a[@data-ftid='component_pagination-item-next']");


    @Step("get Result Title")
    public ElementsCollection getResultTitle (){
        ElementsCollection elements =$$x(".//*[@data-ftid='bull_title']");
        return  elements;
    }

    @Step("exist Warning Message")
    public Boolean existWarningMessage(){
    return Driver.isElementExists(warningLocator,2);
    }
    @Step("get Text Result Title")
    public List<String> getTextResultTitle(){
        List<String> titles = new ArrayList<>();
        if (Driver.isElementNotExists(warningLocator,2)){
            ElementsCollection elements =  getResultTitle ();
            for (SelenideElement element:elements) {
                titles.add(element.text()) ;
            }
            int pageCount = 1;
            while (Driver.isElementExists(hrefNextLocator,2) && pageCount<=AppConfig.MAX_CHECK_PAGE){
                pageCount++;
                hrefNext.click();
                elements =  getResultTitle ();
                for (SelenideElement element:elements) {
                    titles.add(element.text()) ;
                }
            }
        }
        return titles;
    }

    @Step("get Result Price")
    public ElementsCollection getResultPrice (){
        ElementsCollection elements =$$x(".//*[@data-ftid='bull_price']");
        return  elements;
    }

    @Step("get Text Result Price")
    public List<String> getTextResultPrice(){
        List<String> prices = new ArrayList<>();
        if (Driver.isElementNotExists(warningLocator,2)){
            ElementsCollection elements =  getResultPrice ();
            for (SelenideElement element:elements) {
                prices.add(element.text()) ;
            }
            int pageCount = 1;
            while (Driver.isElementExists(hrefNextLocator,2) && pageCount<=AppConfig.MAX_CHECK_PAGE){
                pageCount++;
                hrefNext.click();
                elements =  getResultPrice ();
                for (SelenideElement element:elements) {
                    prices.add(element.text()) ;
                }
            }
        }
        return prices;
    }
}
