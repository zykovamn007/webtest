package app.pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import helpers.Driver;
import io.qameta.allure.Step;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage {
    private SelenideElement usernameInput = $("#sign");
    private SelenideElement passwordInput = $("#password");
    private SelenideElement signInBtn = $("#signbutton");
    private SelenideElement loginErrorMessage = $("#sign_errors");
    private SelenideElement passwordErrorMessage = $("#sign_errors");
    private String pathScreen = Configuration.reportsFolder  + File.separator  + "screenshots"
            + File.separator;



    @Step("fill Email and password")
    public SmsVerificationPage login(String username, String password){
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);
        signInBtn.click();
        return new SmsVerificationPage();
    }

    @Step("fill Email")
    public LoginPage fillEmail(String  email){
        usernameInput.clear();
        usernameInput.sendKeys(email);
        return  this;
    }
    @Step("fill password")
    public LoginPage fillPassword(String  pass){
        passwordInput.clear();
        passwordInput.sendKeys(pass);
        return  this;
    }

    @Step("click SignInBtn")
    public  LoginPage clickSignInBtn(){
        signInBtn.click();
        return this;
    }

    public String getErrorTextLogin(){
        return  loginErrorMessage.getText();
    }
    public String getErrorTextPass(){
        return  passwordErrorMessage.getText();
    }

    public Boolean checkIsDifferByImage(String nameExpectedScreen) throws IOException {
        Driver.waitVisible(signInBtn,10);
        Screenshot expected = new Screenshot(ImageIO.read(this.getClass().getClassLoader()
                .getResourceAsStream(File.separator +
                "data" + File.separator + nameExpectedScreen)));
        Driver.takeScreenshot("ActualScreenLogin");
        Screenshot actual = new Screenshot(ImageIO.read(
                new File(pathScreen + "ActualScreenLogin.png")));
        ImageDiff diff = new ImageDiffer().makeDiff(expected, actual);
        if (diff.getDiffSize() > 0) {
            File result = new File(pathScreen + "diffScreen.png");
            ImageIO.write(diff.getMarkedImage(), "png", result);
        }

        return  diff.hasDiff();
    }



}
