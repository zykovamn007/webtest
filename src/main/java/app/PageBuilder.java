package app;

import app.pages.*;

public class PageBuilder {
    private PageBuilder() {
    }

    public static MainPage buildMainPage() {
        return new MainPage();
    }
    public static LandingPage buildLandingPage() {
        return new LandingPage();
    }
    public static FiltersPage buildFiltersPage() {
        return new FiltersPage();
    }
    public static ResultPage buildResultPage() { return new ResultPage();}
    public static LoginPage buildLoginPage() { return new LoginPage();}
    public static SmsVerificationPage buildSmsVerificationPage() { return new SmsVerificationPage();}
}
