package app;

import app.pages.*;
import lombok.Data;

@Data
public class App {

    private MainPage mainPage;
    private LandingPage landingPage;
    private FiltersPage filtersPage;
    private ResultPage resultPage;
    private LoginPage loginPage;
    private SmsVerificationPage smsVerificationPage;

    public App() {

        mainPage = PageBuilder.buildMainPage();
        landingPage = PageBuilder.buildLandingPage();
        filtersPage = PageBuilder.buildFiltersPage();
        resultPage = PageBuilder.buildResultPage();
        loginPage = PageBuilder.buildLoginPage();
        smsVerificationPage = PageBuilder.buildSmsVerificationPage();
    }

}
