#webtest (Java + Selenide + jUnit5 + Allure)

Automated testing of the user interface of the website drom.ru/


Run test - `gradle test`


Generate allure report  - `allure serve .\build\allure-results\ `
